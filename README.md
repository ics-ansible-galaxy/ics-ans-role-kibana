# ics-ans-role-kibana

Ansible role to install kibana.

## Role Variables

```yaml
kibana_gpg_key: https://packages.elastic.co/GPG-KEY-elasticsearch

kibana_centos_mirror_url: https://artifacts.elastic.co

kibana_release: 7.x

kibana_version: 7.10.2

kibana_package_name: kibana

kibana_conf_template:
  - name: config
    file: kibana.yml.j2
    dest: /etc/kibana/kibana.yml

kibana_repository:
  - baseurl: "{{ centos_mirror_url }}/packages/{{ kibana_release }}/yum"
    description: "Elastic repository for {{ kibana_release }} packages"
    enabled: true
    gpgcheck: true
    gpgkey: "{{ centos_mirror_url }}/GPG-KEY-elasticsearch"
    name: kibana
    reposdir: /etc/yum.repos.d/

kibana_elasticsearch_host: http://localhost:9200

kibana_server_port: 5601

kibana_server_host: "{% if ansible_fqdn %}{{ ansible_fqdn }}{% elif ansible_default_ipv4.address %}{{ ansible_default_ipv4.address }}{% endif %}"

kibana_opendistro_plugins: true   # set to true to install opendistro plugins

kibana_opendistro_plugins_url: https://d3g5vo6xdbdb9a.cloudfront.net/downloads/kibana-plugins

kibana_opendistro_plugins_list:
  - url: opendistro-security/opendistroSecurityKibana-1.13.0.1.zip
    name: opendistroSecurityKibana
  - url: opendistro-alerting/opendistroAlertingKibana-1.13.0.0.zip
    name: opendistroAlertingKibana
  - url: opendistro-index-management/opendistroIndexManagementKibana-1.13.0.1.zip
    name: opendistroIndexManagementKibana
  - url: opendistro-anomaly-detection/opendistroAnomalyDetectionKibana-1.13.0.0.zip
    name: opendistroAnomalyDetectionKibana
  - url: opendistro-trace-analytics/opendistroTraceAnalyticsKibana-1.13.2.0.zip
    name: opendistroTraceAnalyticsKibana
  - url: opendistro-notebooks/opendistroNotebooksKibana-1.13.2.0.zip
    name: opendistroNotebooksKibana
  - url: opendistro-reports/linux/x64/opendistroReportsKibana-1.13.2.0-linux-x64.zip
    name: opendistroReportsKibana
  - url: opendistro-gantt-chart/opendistroGanttChartKibana-1.13.0.0.zip
    name: opendistroGanttChartKibana


...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-kibana
```

## License

BSD 2-clause
